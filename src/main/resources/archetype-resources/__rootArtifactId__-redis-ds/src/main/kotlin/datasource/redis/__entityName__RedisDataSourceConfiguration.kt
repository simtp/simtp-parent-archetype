/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package ${package}.datasource.redis

import ${package}.api.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.stream.redis.createStreamReceiver
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.ZonedDateTime
import java.time.format.DateTimeFormatter


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}RedisDataSourceConfiguration::class)
annotation class Enable${entityName}RedisDataSource

@Configuration
open class ${entityName}RedisDataSourceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}RedisDataSource(
        reactiveRedisOperations: ReactiveRedisOperations<String, String>
    ): ${entityName}RedisDataSource {
        return ${entityName}RedisDataSource(reactiveRedisOperations)
    }

    @Bean
    open fun ${entityName.toLowerCase()}RedisStreamConsumer(
        redisConnectionFactory: ReactiveRedisConnectionFactory,
        objectMapper: ObjectMapper,
        ${entityName.toLowerCase()}RedisDataSource: ${entityName}RedisDataSource
    ): ${entityName}RedisStreamConsumer {
        return ${entityName}RedisStreamConsumer(
            redisConnectionFactory = redisConnectionFactory,
            objectMapper = objectMapper,
            ${entityName.toLowerCase()}RedisDataSource = ${entityName.toLowerCase()}RedisDataSource
        )
    }
}

class ${entityName}RedisStreamConsumer(
    private val redisConnectionFactory: ReactiveRedisConnectionFactory,
    private val objectMapper: ObjectMapper,
    private val ${entityName.toLowerCase()}RedisDataSource: ${entityName}RedisDataSource
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}RedisStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createStreamReceiver(
            connectionFactory = redisConnectionFactory,
            streamName = ${entityName.toUpperCase()}_STREAM_NAME
        ).subscribe { record ->
            ${entityName.toLowerCase()}StreamHandler(
                record.value,
                ${entityName.toLowerCase()}RedisDataSource,
                objectMapper,
                log
            )
        }
    }
}

class ${entityName}RedisDataSource(
    private val reactiveRedisOperations: ReactiveRedisOperations<String, String>
): I${entityName}DataSource {


    override fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        val ops = reactiveRedisOperations
            .opsForHash<String, String>()

        val existing = ops
            .multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", listOf("version"))
            .cache()

        val new${entityName} = ${entityName.toLowerCase()}.cloneAndIncrementVersion()


        return existing
            .filter {
                val result = it.filterNotNull().isNotEmpty()
                //This isn't very elegant
                if (result && it[0].toLong() != ${entityName.toLowerCase()}.version)
                    throw OptimisticLockingFailureException("Current version=${it[0]} does not match provided version=${${entityName.toLowerCase()}.version}")
                result
            }
            .hasElement()
            .flatMap {
                when (it) {
                    true -> {
                        //Exists, now check the version

                        ops.save${entityName}(new${entityName})
                    }
                    false -> ops.save${entityName}(${entityName.toLowerCase()})
                }
            }
    }

    override fun findById(id: String): Mono<${entityName}> {
        return reactiveRedisOperations
            .opsForHash<String, String>()
            .multiGet("${entityName.toLowerCase()}:${id}", all${entityName}Fields)
            .map { it.to${entityName}() }
    }

    override fun findAll(): Flux<${entityName}> {
        return reactiveRedisOperations
            .opsForHash<String, String>()
            .keys("${entityName.toLowerCase()}:*")
            .flatMap { key ->
                reactiveRedisOperations
                    .opsForHash<String, String>()
                    .multiGet(key, all${entityName}Fields)
                    .map { it.to${entityName}() }
            }
    }

    override fun delete(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        val value = findById(${entityName.toLowerCase()}.id).cache()

        val deleteOp = reactiveRedisOperations
            .opsForHash<String, String>()
            .delete("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}")

        return value
            .handle { it, sink ->
                if (it.version != ${entityName.toLowerCase()}.version) {
                    sink.error(OptimisticLockingFailureException("Provided ${entityName.toLowerCase()}=${${entityName.toLowerCase()}} version does not latest ${entityName.toLowerCase()}=${it} version"))
                    return@handle
                }
                sink.next(it)
            }
            .thenReturn(${entityName.toLowerCase()})
            .then(deleteOp)
            .flatMap { value }
    }
}

//TODO - Add additional fields here
internal val all${entityName}Fields = listOf("id", "version", "updated")

internal val zonedDateFormatter = DateTimeFormatter.ISO_ZONED_DATE_TIME

fun ReactiveHashOperations<String, String, String>.save${entityName}(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
    return this
        .putAll("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", ${entityName.toLowerCase()}.toMap())
        .map { ${entityName.toLowerCase()} }
}

fun List<String>.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this[0],
        version = this[1].toLong(),
        updated = ZonedDateTime.parse(this[2], DateTimeFormatter.ISO_ZONED_DATE_TIME),
        //TODO - Map Additional fields here
    )
}


internal val dateFormatter = DateTimeFormatter.ISO_ZONED_DATE_TIME

fun ${entityName}.toMap(): Map<String, String> {
    return mapOf(
        "id" to this.id,
        "version" to this.version.toString(),
        "updated" to zonedDateFormatter.format(this.updated),
        //TODO - Map Additional fields here
    )
}