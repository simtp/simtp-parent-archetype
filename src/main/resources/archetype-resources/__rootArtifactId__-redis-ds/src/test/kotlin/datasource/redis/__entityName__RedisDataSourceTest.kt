/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.datasource.redis

import ${package}.api.*
import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.core.ReactiveHashOperations
import org.springframework.data.redis.core.ReactiveRedisOperations
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Suppress("ReactiveStreamsUnusedPublisher")
class ${entityName}RedisDataSourceTest {

    @Test
    fun save() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}Map = ${entityName.toLowerCase()}.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", ${entityName.toLowerCase()}Map) } returns Mono.just(true)
        every { opsForHash.multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", listOf("version")) } returns Mono.just(listOf(null))

        val ${entityName.toLowerCase()}DataSource = ${entityName}RedisDataSource(reactiveRedisOperations)

        val mono = ${entityName.toLowerCase()}DataSource.save(${entityName.toLowerCase()}).cache()

        StepVerifier
            .create(mono)
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun update() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val new${entityName} = ${entityName.toLowerCase()}.cloneAndIncrementVersion()
        val ${entityName.toLowerCase()}Map = new${entityName}.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", ${entityName.toLowerCase()}Map) } returns Mono.just(true)
        every { opsForHash.multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", listOf("version")) } returns Mono.just(listOf("0"))

        val ${entityName.toLowerCase()}DataSource = ${entityName}RedisDataSource(reactiveRedisOperations)

        val mono = ${entityName.toLowerCase()}DataSource.save(${entityName.toLowerCase()}).cache()

        StepVerifier
            .create(mono)
            .expectNext(new${entityName})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun optimisticLockFailure() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val new${entityName} = ${entityName.toLowerCase()}.cloneAndIncrementVersion()
        val ${entityName.toLowerCase()}Map = new${entityName}.toMap()

        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.putAll("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", ${entityName.toLowerCase()}Map) } returns Mono.just(true)
        every { opsForHash.multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", listOf("version")) } returns Mono.just(listOf("1"))

        val ${entityName.toLowerCase()}DataSource = ${entityName}RedisDataSource(reactiveRedisOperations)

        val mono = ${entityName.toLowerCase()}DataSource.save(${entityName.toLowerCase()}).cache()

        StepVerifier
            .create(mono)
            .expectError(OptimisticLockingFailureException::class.java)
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", all${entityName}Fields) } returns Mono.just(listOf(${entityName.toLowerCase()}.id, ${entityName.toLowerCase()}.version.toString(), zonedDateFormatter.format(${entityName.toLowerCase()}.updated)))
        val ${entityName.toLowerCase()}DataSource = ${entityName}RedisDataSource(reactiveRedisOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findById(${entityName.toLowerCase()}.id))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.keys("${entityName.toLowerCase()}:*") } returns Flux.just("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}")
        every { opsForHash.multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", all${entityName}Fields) } returns Mono.just(listOf(${entityName.toLowerCase()}.id, ${entityName.toLowerCase()}.version.toString(), zonedDateFormatter.format(${entityName.toLowerCase()}.updated)))
        val ${entityName.toLowerCase()}DataSource = ${entityName}RedisDataSource(reactiveRedisOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findAll())
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }


    @Test
    fun delete() {
        val reactiveRedisOperations = mockk<ReactiveRedisOperations<String, String>>(relaxed = true)
        val opsForHash = mockk<ReactiveHashOperations<String, String, String>>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        every { reactiveRedisOperations.opsForHash<String, String>() } returns opsForHash
        every { opsForHash.delete("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}") } returns Mono.just(true)
        every { opsForHash.multiGet("${entityName.toLowerCase()}:${${entityName.toLowerCase()}.id}", all${entityName}Fields) } returns Mono.just(listOf(${entityName.toLowerCase()}.id, ${entityName.toLowerCase()}.version.toString(), zonedDateFormatter.format(${entityName.toLowerCase()}.updated)))

        val ${entityName.toLowerCase()}DataSource = ${entityName}RedisDataSource(reactiveRedisOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.delete(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun testToMap() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val map = ${entityName.toLowerCase()}.toMap()
        val expected = mapOf("id" to ${entityName.toLowerCase()}.id, "version" to ${entityName.toLowerCase()}.version.toString(), "updated" to zonedDateFormatter.format(${entityName.toLowerCase()}.updated))
        assertEquals(expected, map)
    }

    @Test
    fun testTo${entityName}FromStringList() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val fields = ${entityName.toLowerCase()}.toMap().values.toList()
        assertEquals(${entityName.toLowerCase()}, fields.to${entityName}())
    }

}