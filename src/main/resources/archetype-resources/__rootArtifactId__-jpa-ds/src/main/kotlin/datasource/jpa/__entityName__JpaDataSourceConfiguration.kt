/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection", "JpaDataSourceORMInspection")
package ${package}.datasource.jpa

import ${package}.api.*
import jakarta.persistence.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.stream.redis.createStreamReceiver
import org.slf4j.LoggerFactory
import org.springframework.boot.autoconfigure.domain.EntityScan
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.jpa.repository.config.EnableJpaRepositories
import org.springframework.stereotype.Repository
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import reactor.core.scheduler.Schedulers
import org.simtp.kotlin.util.uuid
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.data.jpa.repository.Lock
import org.springframework.data.repository.CrudRepository
import java.util.*
import java.time.ZonedDateTime

@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}JpaDataSourceConfiguration::class)
annotation class Enable${entityName}JpaDataSource

@Repository
interface ${entityName}Repository: CrudRepository<${entityName}Entity, String> {

    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
    override fun <S : ${entityName}Entity> save(entity: S): S

    @Lock(LockModeType.OPTIMISTIC_FORCE_INCREMENT)
    override fun <S : ${entityName}Entity?> saveAll(entities: Iterable<S>): Iterable<S>

    @Lock(LockModeType.OPTIMISTIC)
    override fun findById(id: String): Optional<${entityName}Entity>

    @Lock(LockModeType.OPTIMISTIC)
    override fun existsById(id: String): Boolean

    @Lock(LockModeType.OPTIMISTIC)
    override fun findAll(): Iterable<${entityName}Entity>

    @Lock(LockModeType.OPTIMISTIC)
    override fun findAllById(ids: Iterable<String>): Iterable<${entityName}Entity>

    @Lock(LockModeType.OPTIMISTIC)
    override fun deleteById(id: String)

    @Lock(LockModeType.OPTIMISTIC)
    override fun delete(entity: ${entityName}Entity)

    @Lock(LockModeType.OPTIMISTIC)
    override fun deleteAllById(ids: Iterable<String>)

    @Lock(LockModeType.OPTIMISTIC)
    override fun deleteAll(entities: Iterable<${entityName}Entity>)

    @Lock(LockModeType.OPTIMISTIC)
    override fun deleteAll()
}
@Configuration
@EnableJpaRepositories
@EntityScan
open class ${entityName}JpaDataSourceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}JpaDataSource(repository: ${entityName}Repository): ${entityName}JpaDataSource {
        return ${entityName}JpaDataSource(repository)
    }

    @Bean
    open fun ${entityName.toLowerCase()}JpaStreamConsumer(
        redisConnectionFactory: ReactiveRedisConnectionFactory,
        objectMapper: ObjectMapper,
        ${entityName.toLowerCase()}JpaDataSource: ${entityName}JpaDataSource
    ): ${entityName}JpaStreamConsumer {
        return ${entityName}JpaStreamConsumer(
            redisConnectionFactory = redisConnectionFactory,
            objectMapper = objectMapper,
            ${entityName.toLowerCase()}JpaDataSource = ${entityName.toLowerCase()}JpaDataSource
        )
    }
}

class ${entityName}JpaStreamConsumer(
    private val redisConnectionFactory: ReactiveRedisConnectionFactory,
    private val objectMapper: ObjectMapper,
    private val ${entityName.toLowerCase()}JpaDataSource: ${entityName}JpaDataSource
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}JpaStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createStreamReceiver(
            connectionFactory = redisConnectionFactory,
            streamName = ${entityName.toUpperCase()}_STREAM_NAME
        ).subscribe { record ->
            ${entityName.toLowerCase()}StreamHandler(
                record.value,
                ${entityName.toLowerCase()}JpaDataSource,
                objectMapper,
                log
            )
        }
    }
}


class ${entityName}JpaDataSource(
    private val repository: ${entityName}Repository
): I${entityName}DataSource {

    private fun save${entityName}(${entityName.toLowerCase()}: ${entityName}): ${entityName} {
        return repository
            .save(${entityName.toLowerCase()}.to${entityName}Entity())
            .to${entityName}()
    }

    override fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        return Mono.fromCallable{ save${entityName}(${entityName.toLowerCase()}) }
            .subscribeOn(Schedulers.boundedElastic())
    }

    override fun findById(id: String): Mono<${entityName}> {
        return Mono.fromSupplier {
            repository
                .findById(id)
                .map { it.to${entityName}() }
        }
        .subscribeOn(Schedulers.boundedElastic())
        .flatMap { Mono.justOrEmpty(it) }
    }

    override fun findAll(): Flux<${entityName}> {
        return Mono.fromSupplier {
            repository
                .findAll()
                .map { it.to${entityName}() }
        }
        .subscribeOn(Schedulers.boundedElastic())
        .flatMapIterable { it }

    }

    override fun delete(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        return Mono.fromSupplier {
            repository.delete(${entityName.toLowerCase()}.to${entityName}Entity())
            ${entityName.toLowerCase()}
        }.subscribeOn(Schedulers.boundedElastic())
        .flatMap { Mono.justOrEmpty(it) }
    }
}


@Entity
@Table(name = "${entityName.toLowerCase()}")
open class ${entityName}Entity(
    @Id
    open var id : String = uuid(),

    open var updated: ZonedDateTime = ZonedDateTime.now(),

    @Version
    open var version: Long = 0
) {

    //TODO Update with new fields
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}Entity

        if (id != other.id) return false
        if (updated != other.updated) return false
        if (version != other.version) return false

        return true
    }

    //TODO Update with new fields
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + updated.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }
}

internal fun ${entityName}.to${entityName}Entity(): ${entityName}Entity {
    return ${entityName}Entity(
        id = this.id,
        updated = this.updated,
        version = this.version
    )
}

internal fun ${entityName}Entity.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id,
        updated = this.updated,
        version = this.version
    )
}
