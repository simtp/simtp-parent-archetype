
/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.springframework.context.annotation.*
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}QueryWebServerConfiguration::class)
annotation class Enable${entityName}QueryWebServer

@Configuration
@ComponentScan
open class ${entityName}QueryWebServerConfiguration

@RestController
@RequestMapping("/api/${entityName.toLowerCase()}/1.0")
open class ${entityName}QueryWebRestController(
    private val ${entityName.toLowerCase()}QueryApi: I${entityName}QueryApi
): I${entityName}QueryApi {

        @GetMapping("/{id}")
        override fun get(@PathVariable id: String): Mono<${entityName}> {
            return ${entityName.toLowerCase()}QueryApi[id]
        }
}

