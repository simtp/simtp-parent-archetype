/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.springframework.context.annotation.*
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono

@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}CommandWebServerConfiguration::class)
annotation class Enable${entityName}CommandWebServer

@Configuration
@ComponentScan
open class ${entityName}CommandWebServerConfiguration

@RestController
@RequestMapping("/api/${entityName.toLowerCase()}/1.0")
open class ${entityName}CommandWebRestController(
    private val ${entityName.toLowerCase()}CommandApi: I${entityName}CommandApi
): I${entityName}CommandApi {

        @PutMapping
        override fun create(@RequestBody ${entityName.toLowerCase()}: ${entityName}): Mono<String> {
            return ${entityName.toLowerCase()}CommandApi.create(${entityName.toLowerCase()})
        }

        @PostMapping
        override fun update(@RequestBody ${entityName.toLowerCase()}: ${entityName}): Mono<String> {
            return ${entityName.toLowerCase()}CommandApi.update(${entityName.toLowerCase()})
        }

        @DeleteMapping("/{id}")
        override fun delete(@PathVariable id: String): Mono<String> {
            return ${entityName.toLowerCase()}CommandApi.delete(id)
        }
}

