/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import org.springframework.context.annotation.Bean
import org.springframework.web.reactive.function.client.bodyToMono
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class ${entityName}CommandWebRestControllerIntTest {

    @LocalServerPort
    private var localServerPort: Int? = null

    private var webClient = defaultWebClientBuilder.baseUrl("http://localhost:8080/api/${entityName.toLowerCase()}/1.0").build()

    @Autowired
    private lateinit var ${entityName.toLowerCase()}InMemoryService: ${entityName}InMemoryService

    @BeforeEach
    fun setUp() {
         webClient = defaultWebClientBuilder
             .baseUrl("http://localhost:$localServerPort/api/${entityName.toLowerCase()}/1.0")
             .build()
    }

    @Test
    fun create() {
        val ${entityName.toLowerCase()} = ${entityName}()

        val retrieveMono = webClient
            .put()
            .bodyValue(${entityName.toLowerCase()})
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()}.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(${entityName.toLowerCase()}, ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}.id]?.withLocalZonedDateTime())

    }

    @Test
    fun update() {
        val ${entityName.toLowerCase()} = ${entityName}()
        ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}.id] = ${entityName.toLowerCase()}
        val retrieveMono = webClient
            .post()
            .bodyValue(${entityName.toLowerCase()})
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()}.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(${entityName.toLowerCase()}, ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}.id]?.withLocalZonedDateTime())

    }

    @Test
    fun delete() {
        val ${entityName.toLowerCase()} = ${entityName}()
        ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}.id] = ${entityName.toLowerCase()}
        val retrieveMono = webClient
            .delete()
            .uri("/{id}", mapOf("id" to ${entityName.toLowerCase()}.id))
            .retrieve()
            .bodyToMono<String>()

        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()}.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertNull(${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}.id]?.withLocalZonedDateTime())

    }

}