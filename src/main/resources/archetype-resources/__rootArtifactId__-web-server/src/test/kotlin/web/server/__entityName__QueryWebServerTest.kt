/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import io.mockk.every
import io.mockk.mockk
import io.mockk.verify
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

class ${entityName}QueryWebRestControllerTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun get() {
        val service = mockk<I${entityName}QueryApi>()
        val ${entityName.toLowerCase()} = ${entityName}()
        every { service[${entityName.toLowerCase()}.id] } returns Mono.just(${entityName.toLowerCase()})

        val controller = ${entityName}QueryWebRestController(service)

        StepVerifier
            .create(controller[${entityName.toLowerCase()}.id])
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { service[${entityName.toLowerCase()}.id] }

    }

}