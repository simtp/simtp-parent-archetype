/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.server

import ${package}.api.*
import org.junit.jupiter.api.Assertions.*
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.context.annotation.Bean
import reactor.core.publisher.Mono


@SpringBootApplication
@Enable${entityName}CommandWebServer
@Enable${entityName}QueryWebServer
open class ${entityName}WebServerTestApp {

    @Bean
    open fun ${entityName.toLowerCase()}InMemoryService(): ${entityName}InMemoryService {
        return ${entityName}InMemoryService()
    }

}

open class ${entityName}InMemoryService(
    val map: MutableMap<String, ${entityName}> = mutableMapOf()
): I${entityName}CommandApi, I${entityName}QueryApi {

    override fun get(id: String): Mono<${entityName}> {
        return Mono.justOrEmpty(map[id])
    }

    override fun create(${entityName.toLowerCase()}: ${entityName}): Mono<String> {
        return Mono.fromCallable {
            map[${entityName.toLowerCase()}.id] = ${entityName.toLowerCase()}
            ${entityName.toLowerCase()}.id
        }
    }

    override fun update(${entityName.toLowerCase()}: ${entityName}): Mono<String> {
        return Mono.fromCallable {
            map[${entityName.toLowerCase()}.id] = ${entityName.toLowerCase()}
            ${entityName.toLowerCase()}.id
        }

    }

    override fun delete(id: String): Mono<String> {
        return Mono.fromCallable {
            map.remove(id)?.id
        }

    }
}
