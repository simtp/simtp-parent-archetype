/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.api

import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import org.simtp.kotlin.util.uuid
import org.simtp.stream.api.StreamMessageType
import org.slf4j.Logger
import com.fasterxml.jackson.databind.ObjectMapper
import java.time.ZonedDateTime
import java.time.ZoneId

const val ${entityName.toUpperCase()}_STREAM_NAME = "${entityName.toLowerCase()}"


interface I${entityName}CommandApi {
    fun create(${entityName.toLowerCase()}: ${entityName}): Mono<String>

    fun update(${entityName.toLowerCase()}: ${entityName}): Mono<String>

    fun delete(id: String) : Mono<String>
}

interface I${entityName}QueryApi {
    operator fun get(id: String): Mono<${entityName}>
}

data class ${entityName}(
    val id: String = uuid(),
    //TODO - Add additional fields here
    val updated: ZonedDateTime = ZonedDateTime.now(),
    val version: Long = 0
)

fun ${entityName}.cloneAndIncrementVersion(): ${entityName} {
    return ${entityName}(
        id = this.id,
        version = this.version + 1,
        updated = this.updated
        //TODO - Map additional fields here

    )
}

fun ${entityName}.withLocalZonedDateTime(): ${entityName} {
    return ${entityName}(
        id = this.id,
        version = this.version,
        updated = this.updated.withZoneSameInstant(java.time.ZoneId.systemDefault())
    )
}

interface I${entityName}DataSource {

    fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}>

    fun findById(id: String): Mono<${entityName}>

    fun findAll(): Flux<${entityName}>

    fun delete(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}>

}

val ${entityName.toLowerCase()}StreamHandler:
            (Map<String, String>, I${entityName}DataSource, ObjectMapper, Logger) -> Mono<${entityName}> = { record, ds, objectMapper, log ->

    when (record["type"]) {
        StreamMessageType.CREATE.name, StreamMessageType.UPDATE.name -> {
            objectMapper
                .readValue(record["json"], ${entityName}::class.java)
                ?.let { ds.save(it) }
                ?: Mono.empty()
        }
        StreamMessageType.DELETE.name -> {
            log.info("Received messageType={}", StreamMessageType.DELETE.name)
            Mono.empty()
        }
        else -> {
            log.error("Unexpected record type={}", record["type"])
            Mono.empty()
        }
    }

}