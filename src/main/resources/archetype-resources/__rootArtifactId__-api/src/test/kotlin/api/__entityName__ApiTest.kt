/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.api

import io.mockk.mockk
import java.time.Duration
import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.framework.serialization.withDefaults

import org.junit.jupiter.api.Test
import reactor.test.StepVerifier

internal class I${entityName}CommandApiTest {

    @Test
    fun test${entityName}StreamHandler() {
        val ${entityName.toLowerCase()}DataSource = mockk<I${entityName}DataSource>(relaxed = true)

        val mono = ${entityName.toLowerCase()}StreamHandler(
            emptyMap(),
            ${entityName.toLowerCase()}DataSource,
            ObjectMapper().withDefaults(),
            mockk(relaxed = true)
        )

        StepVerifier
            .create(mono)
            .expectNext()
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }
}