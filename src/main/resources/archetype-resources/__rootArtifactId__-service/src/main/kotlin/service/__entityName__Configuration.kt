/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import com.fasterxml.jackson.databind.ObjectMapper
import ${package}.api.*
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.ComponentScan
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.redis.core.ReactiveStreamOperations


@Configuration
open class ${entityName}CommandServiceConfiguration {

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun ${entityName.toLowerCase()}CommandService(
        objectMapper: ObjectMapper,
        reactiveStreamOperations: ReactiveStreamOperations<String, String, String>
    ): I${entityName}CommandApi {
        return ${entityName}CommandService(
            objectMapper = objectMapper,
            reactiveStreamOperations = reactiveStreamOperations,
        )
    }

}

@Configuration
open class ${entityName}QueryServiceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}QueryService(
    ${entityName.toLowerCase()}DataSource: I${entityName}DataSource,
    ): I${entityName}QueryApi {
        return ${entityName}QueryService(
            ${entityName.toLowerCase()}DataSource = ${entityName.toLowerCase()}DataSource
        )
    }

}


@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}CommandServiceConfiguration::class)
annotation class Enable${entityName}CommandService

@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}QueryServiceConfiguration::class)
annotation class Enable${entityName}QueryService