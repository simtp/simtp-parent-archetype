/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import com.fasterxml.jackson.databind.ObjectMapper
import ${package}.api.*
import org.simtp.stream.api.StreamMessageType
import org.springframework.data.redis.connection.stream.StreamRecords
import org.springframework.data.redis.core.ReactiveStreamOperations
import reactor.core.publisher.Mono


class ${entityName}CommandService(
    private val reactiveStreamOperations: ReactiveStreamOperations<String, String, String>,
    private val objectMapper: ObjectMapper
): I${entityName}CommandApi {


    override fun create(${entityName.toLowerCase()}: ${entityName}) : Mono<String> {

        val record = StreamRecords.mapBacked<String, String, String>(mapOf(
            "event-type" to StreamMessageType.CREATE.name,
            "json" to objectMapper.writeValueAsString(${entityName.toLowerCase()})
        ))
            .withStreamKey(CAMERA_STREAM_NAME)

        return reactiveStreamOperations
            .add(record)
            .map { ${entityName.toLowerCase()}.id }

    }

    override fun update(${entityName.toLowerCase()}: ${entityName}) : Mono<String> {

        val record = StreamRecords.mapBacked<String, String, String>(mapOf(
            "event-type" to StreamMessageType.UPDATE.name,
            "json" to objectMapper.writeValueAsString(${entityName.toLowerCase()})
        ))
        .withStreamKey(CAMERA_STREAM_NAME)

        return reactiveStreamOperations
            .add(record)
            .map { ${entityName.toLowerCase()}.id }

    }

    override fun delete(id: String) : Mono<String> {
        val record = StreamRecords.mapBacked<String, String, String>(mapOf(
            "event-type" to StreamMessageType.DELETE.name,
            "id" to id
        ))
        .withStreamKey(CAMERA_STREAM_NAME)

        return reactiveStreamOperations
            .add(record)
            .map { id }


    }
}
