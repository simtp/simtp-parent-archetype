/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import com.fasterxml.jackson.databind.ObjectMapper
import io.mockk.*

import ${package}.api.*
import org.springframework.data.redis.core.ReactiveStreamOperations
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.simtp.framework.serialization.withDefaults
import org.springframework.data.redis.connection.stream.MapRecord
import org.springframework.data.redis.connection.stream.RecordId
import reactor.core.publisher.Mono
import reactor.test.StepVerifier
import java.time.Duration

class ${entityName}CommandServiceTest {

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun create() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val reactiveStreamOperations = mockk<ReactiveStreamOperations<String, String, String>>(relaxed = true)
        every { reactiveStreamOperations.add(any<MapRecord<String, String, String>>()) } returns Mono.just(RecordId.autoGenerate())
        val objectMapper = ObjectMapper().withDefaults()
        val ${entityName.toLowerCase()}Service = ${entityName}CommandService(
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = objectMapper
        )
        StepVerifier
            .create(${entityName.toLowerCase()}Service.create(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()}.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { reactiveStreamOperations.add(any<MapRecord<String, String, String>>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun update() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val reactiveStreamOperations = mockk<ReactiveStreamOperations<String, String, String>>(relaxed = true)
        every { reactiveStreamOperations.add(any<MapRecord<String, String, String>>()) } returns Mono.just(RecordId.autoGenerate())
        val objectMapper = ObjectMapper().withDefaults()
        val ${entityName.toLowerCase()}Service = ${entityName}CommandService(
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = objectMapper
        )
        StepVerifier
            .create(${entityName.toLowerCase()}Service.create(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()}.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { reactiveStreamOperations.add(any<MapRecord<String, String, String>>()) }
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val reactiveStreamOperations = mockk<ReactiveStreamOperations<String, String, String>>(relaxed = true)
        every { reactiveStreamOperations.add(any<MapRecord<String, String, String>>()) } returns Mono.just(RecordId.autoGenerate())
        val objectMapper = ObjectMapper().withDefaults()
        val ${entityName.toLowerCase()}Service = ${entityName}CommandService(
            reactiveStreamOperations = reactiveStreamOperations,
            objectMapper = objectMapper
        )
        StepVerifier
            .create(${entityName.toLowerCase()}Service.delete(${entityName.toLowerCase()}.id))
            .expectNext(${entityName.toLowerCase()}.id)
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { reactiveStreamOperations.add(any<MapRecord<String, String, String>>()) }
    }
}