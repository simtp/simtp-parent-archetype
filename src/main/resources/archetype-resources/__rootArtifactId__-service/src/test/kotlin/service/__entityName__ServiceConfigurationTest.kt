/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import ${package}.datasource.memory.Enable${entityName}MemoryDataSource
import org.junit.jupiter.api.*
import org.junit.jupiter.api.Assertions.*
import org.simtp.framework.serialization.EnableSerialization
import org.simtp.stream.redis.EnableRedisStream
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.testcontainers.service.connection.ServiceConnection
import org.testcontainers.containers.GenericContainer
import org.testcontainers.junit.jupiter.Container
import org.testcontainers.utility.DockerImageName

@SpringBootTest
@EnableRedisStream
@Tag("integration")
internal class ${entityName}CommandServiceConfigurationTest {

    companion object {
        @Container
        @ServiceConnection
        private val redisContainer =
            GenericContainer(DockerImageName.parse("redis:latest"))
                .withExposedPorts(6379)


        @BeforeAll
        @JvmStatic
        fun setUp() {
            redisContainer
                .withReuse(true)
                .start()
        }

        @AfterAll
        @JvmStatic
        fun tearDown() {
            redisContainer.stop()

        }

    }

    @Autowired
    private lateinit var service: ${entityName}CommandService

    @Test
    fun contextLoads(){
        assertNotNull(service)
    }

}

@SpringBootApplication
@Enable${entityName}CommandService
@Enable${entityName}MemoryDataSource
@EnableSerialization
open class ${entityName}OrderServiceTestApp
