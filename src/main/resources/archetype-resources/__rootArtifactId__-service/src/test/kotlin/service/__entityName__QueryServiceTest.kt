/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.service

import ${package}.api.*
import ${package}.datasource.memory.${entityName}MemoryDataSource
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration

class ${entityName}QueryServiceTest {

    @Test
    fun get() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}DataSource = ${entityName}MemoryDataSource()
        ${entityName.toLowerCase()}DataSource.map[${entityName.toLowerCase()}.id] = ${entityName.toLowerCase()}
        val ${entityName.toLowerCase()}QueryService = ${entityName}QueryService(
            ${entityName.toLowerCase()}DataSource = ${entityName.toLowerCase()}DataSource
        )
        StepVerifier
            .create(${entityName.toLowerCase()}QueryService[${entityName.toLowerCase()}.id])
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }


}