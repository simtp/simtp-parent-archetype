/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package ${package}.datasource.mongodb

import ${package}.api.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.stream.redis.createStreamReceiver
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.simtp.kotlin.util.uuid
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import org.springframework.data.annotation.Id
import org.springframework.data.annotation.Version
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import org.springframework.data.mongodb.core.mapping.Document
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono
import java.time.ZonedDateTime
import java.time.LocalDateTime
import java.time.ZoneId


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}MongoDBDataSourceConfiguration::class)
annotation class Enable${entityName}MongoDBDataSource


@Configuration
open class ${entityName}MongoDBDataSourceConfiguration {

    @Bean
    open fun ${entityName.toLowerCase()}MongoDBDataSource(
        reactiveMongoOperations: ReactiveMongoOperations
    ): ${entityName}MongoDBDataSource {
        return ${entityName}MongoDBDataSource(reactiveMongoOperations)
    }

    @Bean
    open fun ${entityName.toLowerCase()}MongoDBStreamConsumer(
        redisConnectionFactory: ReactiveRedisConnectionFactory,
        objectMapper: ObjectMapper,
        ${entityName.toLowerCase()}MongoDBDataSource: ${entityName}MongoDBDataSource
    ): ${entityName}MongoDBStreamConsumer {
        return ${entityName}MongoDBStreamConsumer(
            redisConnectionFactory = redisConnectionFactory,
            objectMapper = objectMapper,
            ${entityName.toLowerCase()}MongoDBDataSource = ${entityName.toLowerCase()}MongoDBDataSource
        )
    }    
}

class ${entityName}MongoDBStreamConsumer(
    private val redisConnectionFactory: ReactiveRedisConnectionFactory,
    private val objectMapper: ObjectMapper,
    private val ${entityName.toLowerCase()}MongoDBDataSource: ${entityName}MongoDBDataSource
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}MongoDBStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createStreamReceiver(
            connectionFactory = redisConnectionFactory,
            streamName = ${entityName.toUpperCase()}_STREAM_NAME
        ).subscribe { record ->
            ${entityName.toLowerCase()}StreamHandler(
                record.value,
                ${entityName.toLowerCase()}MongoDBDataSource,
                objectMapper,
                log
            )
        }
    }
}

class ${entityName}MongoDBDataSource(
    private val reactiveMongoOperations: ReactiveMongoOperations
): I${entityName}DataSource {


    override fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        return reactiveMongoOperations
            .findById(${entityName.toLowerCase()}.id, ${entityName}Document::class.java)
            .handle { it, sink ->
                if (it != null && it.version != ${entityName.toLowerCase()}.version) {
                    sink.error(OptimisticLockingFailureException("Provided ${entityName.toLowerCase()}=${${entityName.toLowerCase()}} version does not latest ${entityName.toLowerCase()}=${it} version"))
                }
                if (it != ${entityName.toLowerCase()}.to${entityName}Document())
                    sink.next(it)
            }
            .defaultIfEmpty(${entityName.toLowerCase()}.to${entityName}Document())
            .flatMap {
                reactiveMongoOperations.save(it)
            }
            .map { it.to${entityName}() }
    }

    override fun findById(id: String): Mono<${entityName}> {
        return reactiveMongoOperations
            .findById(id, ${entityName}Document::class.java)
            .map { it.to${entityName}() }
    }

    override fun findAll(): Flux<${entityName}> {
        return reactiveMongoOperations
            .findAll(${entityName}Document::class.java)
            .map { it.to${entityName}() }

    }

    override fun delete(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        return reactiveMongoOperations
            .findById(${entityName.toLowerCase()}.id, ${entityName}Document::class.java)
            .handle { it, sink ->
                if (it != null && it.version != ${entityName.toLowerCase()}.version) {
                    sink.error(OptimisticLockingFailureException("Provided ${entityName.toLowerCase()}=${${entityName.toLowerCase()}} version does not latest ${entityName.toLowerCase()}=${it} version"))
                } else {
                    sink.next(it)
                }
            }
            .defaultIfEmpty(${entityName.toLowerCase()}.to${entityName}Document())
            .flatMap {
                reactiveMongoOperations.remove(it)
            }
            .map { ${entityName.toLowerCase()} }
    }
}

internal fun ${entityName}Document.to${entityName}(): ${entityName} {
    return ${entityName}(
        id = this.id,
        updated = this.updated.atZone(java.time.ZoneId.of("UTC")).withZoneSameInstant(java.time.ZoneId.systemDefault()),
        version = this.version,
        //TODO - Map additional fields here
    )
}

internal fun ${entityName}.to${entityName}Document(): ${entityName}Document {
    return ${entityName}Document(
        id = this.id,
        updated = this.updated.withZoneSameInstant(ZoneId.of("UTC")).toLocalDateTime(),
        version = this.version,
        //TODO - Map additional fields here
    )
}

@Document("${entityName.toLowerCase()}")
open class ${entityName}Document(
    @Id
    var id: String = uuid(),

    var updated: LocalDateTime = LocalDateTime.now(ZoneId.of("UTC")),

    @Version
    var version: Long = 0
) {
    //TODO Update with new fields
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ${entityName}Document

        if (id != other.id) return false
        if (updated != other.updated) return false
        if (version != other.version) return false

        return true
    }

    //TODO Update with new fields
    override fun hashCode(): Int {
        var result = id.hashCode()
        result = 31 * result + updated.hashCode()
        result = 31 * result + version.hashCode()
        return result
    }
}