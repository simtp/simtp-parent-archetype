/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.datasource.mongodb

import com.mongodb.client.result.DeleteResult
import io.mockk.*
import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import ${package}.api.${entityName}
import org.junit.jupiter.api.Assertions.*
import org.springframework.data.mongodb.core.ReactiveMongoOperations
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono

@Suppress("ReactiveStreamsUnusedPublisher")
class ${entityName}MongoDBDataSourceTest {

    @Test
    fun save() {
        val reactiveMongoOperations = mockk<ReactiveMongoOperations>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}Document = ${entityName.toLowerCase()}.to${entityName}Document()
        every { reactiveMongoOperations.findById(${entityName.toLowerCase()}Document.id, ${entityName}Document::class.java) } returns Mono.empty()
        every { reactiveMongoOperations.save(${entityName.toLowerCase()}Document) } returns Mono.just(${entityName.toLowerCase()}Document)

        val ${entityName.toLowerCase()}DataSource = ${entityName}MongoDBDataSource(reactiveMongoOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.save(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

    }

    @Test
    fun findOne() {
        val reactiveMongoOperations = mockk<ReactiveMongoOperations>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        every { reactiveMongoOperations.findById(${entityName.toLowerCase()}.id, ${entityName}Document::class.java) }.returns(Mono.just(${entityName.toLowerCase()}.to${entityName}Document()))
        val ${entityName.toLowerCase()}DataSource = ${entityName}MongoDBDataSource(reactiveMongoOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findById(${entityName.toLowerCase()}.id))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val reactiveMongoOperations = mockk<ReactiveMongoOperations>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        every { reactiveMongoOperations.findAll(${entityName}Document::class.java) }.returns(Flux.fromIterable(setOf(${entityName.toLowerCase()}.to${entityName}Document())))
        val ${entityName.toLowerCase()}DataSource = ${entityName}MongoDBDataSource(reactiveMongoOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findAll())
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Suppress("ReactiveStreamsUnusedPublisher")
    @Test
    fun delete() {
        val reactiveMongoOperations = mockk<ReactiveMongoOperations>(relaxed = true)
        val ${entityName.toLowerCase()} = ${entityName}()
        val document = ${entityName.toLowerCase()}.to${entityName}Document()
        every { reactiveMongoOperations.findById(${entityName.toLowerCase()}.id, ${entityName}Document::class.java) }
            .returns( Mono.just(${entityName.toLowerCase()}.to${entityName}Document()))

        every { reactiveMongoOperations.remove(document) } returns Mono.just( DeleteResult.acknowledged(1) )
        val ${entityName.toLowerCase()}DataSource = ${entityName}MongoDBDataSource(reactiveMongoOperations)

        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.delete(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        verify(exactly = 1) { reactiveMongoOperations.remove(document) }
    }

    @Test
    fun testMapping() {
        val ${entityName.toLowerCase()} = ${entityName}()
        assertEquals(${entityName.toLowerCase()}, ${entityName.toLowerCase()}.to${entityName}Document().to${entityName}())
    }
}