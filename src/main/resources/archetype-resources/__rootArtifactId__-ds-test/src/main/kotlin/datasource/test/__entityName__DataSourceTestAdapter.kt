/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.datasource.test

import ${package}.api.${entityName}
import ${package}.api.I${entityName}DataSource
import java.time.Duration
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.dao.OptimisticLockingFailureException
import java.time.ZonedDateTime

open class ${entityName}DataSourceTestAdapter {

    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Autowired
    private lateinit var ${entityName.toLowerCase()}DataSource: I${entityName}DataSource

    @Test
    fun contextLoads() {
        assertNotNull(${entityName.toLowerCase()}DataSource)
    }


    @Test
    fun optimisticUpdatePasses() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val result1 = ${entityName.toLowerCase()}DataSource
            .save(${entityName.toLowerCase()})
            .block(Duration.ofSeconds(5))

        val newValue = ${entityName}(
            id = result1!!.id,
            updated = ZonedDateTime.now(),
            version = result1.version
        )

        val result2 = ${entityName.toLowerCase()}DataSource
            .save(newValue)
            .block(Duration.ofSeconds(5))

        assertEquals(result1.version+1, result2!!.version)
    }

    @Test
    fun optimisticDeleteFails() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val result1 = ${entityName.toLowerCase()}DataSource
            .save(${entityName.toLowerCase()})
            .block(Duration.ofSeconds(5))

        val newValue = ${entityName}(
            id = result1!!.id,
            updated = ZonedDateTime.now(),
            version = result1.version
        )

        val result2 = ${entityName.toLowerCase()}DataSource
            .save(newValue)
            .block(Duration.ofSeconds(5))

        assertEquals(result1.version+1, result2!!.version)

        val otherValue = ${entityName}(
            id = result1.id,
            updated = ZonedDateTime.now(),
            version = result1.version
        )

        assertThrows(OptimisticLockingFailureException::class.java) {
            ${entityName.toLowerCase()}DataSource
                .delete(otherValue)
                .block(Duration.ofSeconds(5))
        }

    }

}