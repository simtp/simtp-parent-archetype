/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.client

import ${package}.api.*
import org.springframework.context.annotation.*
import org.springframework.web.bind.annotation.*
import reactor.core.publisher.Mono
import org.springframework.context.annotation.Bean
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.web.reactive.function.client.WebClient

@Suppress("unused")
@MustBeDocumented
@Target(AnnotationTarget.CLASS)
@Retention(AnnotationRetention.RUNTIME)
@Import(${entityName}CommandWebClientConfiguration::class)
annotation class Enable${entityName}CommandWebClient

@Configuration
open class ${entityName}CommandWebClientConfiguration {


    @Suppress("SpringJavaInjectionPointsAutowiringInspection")
    @Bean
    open fun ${entityName.toLowerCase()}CommandWebClient(
        webClientBuilder: WebClient.Builder,
        ${entityName.toLowerCase()}WebClientSettings: ${entityName}WebClientSettings
    ): ${entityName}CommandWebClient {
        return ${entityName}CommandWebClient(
            webClientBuilder = webClientBuilder,
            baseUrlProvider = { ${entityName.toLowerCase()}WebClientSettings.baseUrl }
        )
    }
}

data class ${entityName}CommandWebClientSettings(
    var baseUrl: String = "http://localhost:8080"
)

open class ${entityName}CommandWebClient(
    webClientBuilder: WebClient.Builder,
    baseUrlProvider: () -> String
): I${entityName}CommandApi {

    private val webClient = webClientBuilder.baseUrl(baseUrlProvider()).build()

//    override fun get(id: String): Mono<${entityName}> {
//        return webClient
//            .get()
//            .uri("/{id}", mapOf("id" to id))
//            .retrieve()
//            .bodyToMono(${entityName}::class.java)
//    }

    override fun create(${entityName.toLowerCase()}: ${entityName}): Mono<String> {
        return webClient
            .put()
            .bodyValue(${entityName.toLowerCase()})
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun update(${entityName.toLowerCase()}: ${entityName}): Mono<String> {
        return webClient
            .post()
            .bodyValue(${entityName.toLowerCase()})
            .retrieve()
            .bodyToMono(String::class.java)
    }

    override fun delete(id: String): Mono<String> {
        return webClient
            .delete()
            .uri("/{id}", mapOf("id" to id))
            .retrieve()
            .bodyToMono(String::class.java)
    }
}

