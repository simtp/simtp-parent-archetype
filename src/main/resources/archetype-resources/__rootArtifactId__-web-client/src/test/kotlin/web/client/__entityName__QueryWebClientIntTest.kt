/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.web.client

import ${package}.api.*
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.Test
import org.simtp.framework.test.serialization.defaultWebClientBuilder
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.server.LocalServerPort
import reactor.test.StepVerifier
import java.time.Duration
import org.junit.jupiter.api.BeforeEach


@SpringBootTest(
    webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT
)
open class ${entityName}QueryWebClientIntTest {

    @Autowired
    private lateinit var ${entityName.toLowerCase()}InMemoryService: ${entityName}InMemoryService

    @LocalServerPort
    private val localServerPort: Int? = null

    private var ${entityName.toLowerCase()}QueryWebClient: ${entityName}QueryWebClient? = null

    @BeforeEach
    fun setUp() {
        ${entityName.toLowerCase()}QueryWebClient= ${entityName}QueryWebClient(
            webClientBuilder = defaultWebClientBuilder
        ) {
            "http://localhost:${localServerPort}/api/${entityName.toLowerCase()}/1.0"
        }
    }


    @Test
    fun get() {
        val ${entityName.toLowerCase()} = ${entityName}()
        ${entityName.toLowerCase()}InMemoryService.map[${entityName.toLowerCase()}.id] = ${entityName.toLowerCase()}
        val retrieveMono = ${entityName.toLowerCase()}QueryWebClient!![${entityName.toLowerCase()}.id]
        StepVerifier
            .create(retrieveMono)
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))


    }

}
