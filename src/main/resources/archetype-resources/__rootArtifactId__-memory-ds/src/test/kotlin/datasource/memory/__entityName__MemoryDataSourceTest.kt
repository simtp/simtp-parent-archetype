/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
package ${package}.datasource.memory

import org.junit.jupiter.api.Test
import reactor.test.StepVerifier
import java.time.Duration
import ${package}.api.${entityName}
import org.junit.jupiter.api.Assertions.*

@Suppress("ReactiveStreamsUnusedPublisher")
class ${entityName}MemoryDataSourceTest {

    @Test
    fun save() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}DataSource = ${entityName}MemoryDataSource()
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.save(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findOne() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}DataSource = ${entityName}MemoryDataSource(mutableMapOf( ${entityName.toLowerCase()}.id to ${entityName.toLowerCase()} ))
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findById(${entityName.toLowerCase()}.id))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun findAll() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}DataSource = ${entityName}MemoryDataSource(mutableMapOf( ${entityName.toLowerCase()}.id to ${entityName.toLowerCase()} ))
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.findAll())
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))
    }

    @Test
    fun delete() {
        val ${entityName.toLowerCase()} = ${entityName}()
        val ${entityName.toLowerCase()}DataSource = ${entityName}MemoryDataSource(mutableMapOf( ${entityName.toLowerCase()}.id to ${entityName.toLowerCase()} ))
        StepVerifier
            .create(${entityName.toLowerCase()}DataSource.delete(${entityName.toLowerCase()}))
            .expectNext(${entityName.toLowerCase()})
            .expectComplete()
            .verify(Duration.ofSeconds(5))

        assertEquals(0, ${entityName.toLowerCase()}DataSource.map.size)
    }


}