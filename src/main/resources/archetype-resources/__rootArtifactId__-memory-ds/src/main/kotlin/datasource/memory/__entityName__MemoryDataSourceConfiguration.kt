/*
Licensed to the Apache Software Foundation (ASF) under one
or more contributor license agreements.  See the NOTICE file
distributed with this work for additional information
regarding copyright ownership.  The ASF licenses this file
to you under the Apache License, Version 2.0 (the
"License"); you may not use this file except in compliance
with the License.  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing,
software distributed under the License is distributed on an
"AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
KIND, either express or implied.  See the License for the
specific language governing permissions and limitations
under the License.
 */
@file:Suppress("SpringJavaInjectionPointsAutowiringInspection")
package ${package}.datasource.memory

import ${package}.api.*
import com.fasterxml.jackson.databind.ObjectMapper
import org.simtp.stream.redis.createStreamReceiver
import org.slf4j.LoggerFactory
import org.springframework.boot.context.event.ApplicationReadyEvent
import org.springframework.context.ApplicationListener
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.context.annotation.Import
import org.springframework.dao.OptimisticLockingFailureException
import org.springframework.data.redis.connection.ReactiveRedisConnectionFactory
import reactor.core.publisher.Flux
import reactor.core.publisher.Mono


@Retention(AnnotationRetention.RUNTIME)
@Target(AnnotationTarget.CLASS)
@MustBeDocumented
@Import(${entityName}MemoryDataSourceConfiguration::class)
annotation class Enable${entityName}MemoryDataSource


@Configuration
open class ${entityName}MemoryDataSourceConfiguration {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}MemoryDataSourceConfiguration::class.java)
    }

    @Bean
    open fun ${entityName.toLowerCase()}MemoryDataSource(): ${entityName}MemoryDataSource {
        return ${entityName}MemoryDataSource()
    }

    @Bean
    open fun ${entityName.toLowerCase()}MemoryStreamConsumer(
        redisConnectionFactory: ReactiveRedisConnectionFactory,
        objectMapper: ObjectMapper,
        ${entityName.toLowerCase()}MemoryDataSource: ${entityName}MemoryDataSource
    ): ${entityName}MemoryStreamConsumer {
        return ${entityName}MemoryStreamConsumer(
            redisConnectionFactory = redisConnectionFactory,
            objectMapper = objectMapper,
            ${entityName.toLowerCase()}MemoryDataSource = ${entityName.toLowerCase()}MemoryDataSource
        )
    }
}

class ${entityName}MemoryStreamConsumer(
    private val redisConnectionFactory: ReactiveRedisConnectionFactory,
    private val objectMapper: ObjectMapper,
    private val ${entityName.toLowerCase()}MemoryDataSource: ${entityName}MemoryDataSource
): ApplicationListener<ApplicationReadyEvent> {

    companion object {
        private val log = LoggerFactory.getLogger(${entityName}MemoryStreamConsumer::class.java)
    }

    override fun onApplicationEvent(event: ApplicationReadyEvent) {
        createStreamReceiver(
            connectionFactory = redisConnectionFactory,
            streamName = ${entityName.toUpperCase()}_STREAM_NAME
        ).subscribe { record ->
            ${entityName.toLowerCase()}StreamHandler(
                record.value,
                ${entityName.toLowerCase()}MemoryDataSource,
                objectMapper,
                log
            )
        }
    }
}

@Suppress("MemberVisibilityCanBePrivate")
class ${entityName}MemoryDataSource(
    val map: MutableMap<String, ${entityName}> = mutableMapOf()
): I${entityName}DataSource {


    private fun save${entityName}(${entityName.toLowerCase()}: ${entityName}): ${entityName} {
        val existing = map[${entityName.toLowerCase()}.id]
        if (existing != null && existing.version != ${entityName.toLowerCase()}.version)
            throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${${entityName.toLowerCase()}.version}")

        val newVal = when (existing == null) {
            true -> ${entityName.toLowerCase()}
            false -> ${entityName.toLowerCase()}.cloneAndIncrementVersion()

        }
        map[${entityName.toLowerCase()}.id] = newVal

        return newVal

    }

    override fun save(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
            return Mono.just(save${entityName}(${entityName.toLowerCase()}))
    }

    override fun findById(id: String): Mono<${entityName}> {
            return map[id]?.let { Mono.just(it)} ?: Mono.empty()
    }

    override fun findAll(): Flux<${entityName}> {
            return Flux.fromIterable(map.values)

    }

    override fun delete(${entityName.toLowerCase()}: ${entityName}): Mono<${entityName}> {
        val existing = map[${entityName.toLowerCase()}.id]
        if (existing != null && existing.version != ${entityName.toLowerCase()}.version)
            throw OptimisticLockingFailureException("Current version=$existing does not match provided version=${${entityName.toLowerCase()}.version}")

        return map.remove(${entityName.toLowerCase()}.id)?.let { Mono.just(it)} ?: Mono.empty()
    }
}
